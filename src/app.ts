import {MgChatServer} from "@config/setupServer";
import dataBaseConnection from '@config/setupDataBase'
import express, {Express} from 'express'
import {config} from "@config/config";


class Application {
    public init(): void {
        this.loadConfig();
        dataBaseConnection();
        const expressApp: Express = express();
        const MgChatInstance: MgChatServer = new MgChatServer(expressApp);
        MgChatInstance.start();
    }

    private loadConfig(): void {
        config.validateEnvConfig();
    }
}

const application: Application = new Application();
application.init();
