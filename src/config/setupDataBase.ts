import mongoose from 'mongoose';
import process from 'process';
import { config } from '@config/config';
import Logger from 'bunyan';

const LOG: Logger = config.createLogger('dataBaseConnection');
/*
 * function for connect  database
 */
export default () => {
   const connect = async () => {
      try {
         await mongoose.connect(`${config.DATABASE_URL}`);
         LOG.info('Successfully db connection');
      } catch (e) {
         LOG.error('error connecting to database', e);
         return process.exit(1);
      }
   };
   connect().then();
};
